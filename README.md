# Login Workflow Backend

The backend for the [login-workflow] application.
The [login-workflow] is an example of a containerized workflow.

This project requires a linux host with docker and jdk8.

## Integration Test

Run integration tests once:
```
./gradlew iTestRunner
```
The steps performed by the test runner script are:
1. Create docker network all containers will run on
2. Run detached Postgres container
3. Run detached MailHog container
4. Build backend image and run detached container
5. Build test container and run
6. Stop/Delete containers and network
7. Exit the test runner script with the exit status of the test container

The steps outlined above to not lead to rapid development cycles motivating development mode.

## Development Mode Integration Test

To run the tests in development mode:
```
./scripts/dispatcher dev-test-runner
```
This command starts by performing steps 1 - 3 listed above.
It then builds the backend image using the `dev` target of the same multi-stage [Dockerfile].
This `dev` target has `./gradlew quarkusDev` as an entrypoint.
Therefore, changes to the container source code are picked up by the running application.
A detached container is started from this built image with the host source code mounted.
Next, the test image is built as in step 5.
However, the container is started with the host source code mounted.
Additionally, rather than running `./gradlew iTest` once and exiting, the entrypoint is overridden with an interactive shell.
This allows for tests to be run repeatedly (using `./gradlew iTest`) with changes to backend and test source code being picked up.
When finished developing, exit the interactive shell and the containers and network will be stopped/deleted.

## Test Runner Logging

Most logging is suppressed in the test runner scripts such as the output from `docker build`.
To turn this logging on, use environment `LOGIN_WORKFLOW_LOGGING=1`.
So to run tests with docker build logging:
```
LOGIN_WORKFLOW_LOGGING=1 ./gradlew iTestRunner
```

## Docker Image Cleanup

Remove built images:
```
./gradlew rmBuiltImages
```

Remove dependency images:
```
./gradlew rmDependencyImages
```

## Todo

Investigate informing IDEs of the test events from the container.

[login-workflow]: https://gitlab.com/akluball/login-workflow

[Dockerfile]: docker/main/Dockerfile
