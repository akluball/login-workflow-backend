package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.LogoutClient;
import com.gitlab.akluball.lwb.itest.data.UserDao;
import com.gitlab.akluball.lwb.itest.model.Session;
import com.gitlab.akluball.lwb.itest.model.User;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import com.gitlab.akluball.lwb.itest.util.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class LogoutControllerIT {
    private final UniqueData uniqueData;
    private final UniqueBuilder uniqueBuilder;
    private final UserDao userDao;
    private final LogoutClient logoutClient;

    @Guicy
    LogoutControllerIT(UniqueData uniqueData, UniqueBuilder uniqueBuilder, UserDao userDao, LogoutClient logoutClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilder = uniqueBuilder;
        this.userDao = userDao;
        this.logoutClient = logoutClient;
    }

    @Test
    void logout() {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout(user.getId(), session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void logoutNoUserIdLogoutCookie() {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout(null, session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(400);
    }

    @Test
    void logoutNoSessionLogoutCookie() {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout(user.getId(), null);
        assertThat(response.getStatus()).isEqualTo(400);
    }

    @Test
    void logoutUserNotFound() {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().session(session).build();
        Response response = this.logoutClient.logout(user.getId(), session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(404);
    }

    @Test
    void logoutNotUsersSession() {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout(user.getId(), session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(400);
    }
}
