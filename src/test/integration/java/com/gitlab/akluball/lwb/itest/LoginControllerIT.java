package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.LoginClient;
import com.gitlab.akluball.lwb.itest.data.UserDao;
import com.gitlab.akluball.lwb.itest.model.User;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.security.CookieNames;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import com.gitlab.akluball.lwb.itest.util.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class LoginControllerIT {
    private final UniqueData uniqueData;
    private final UniqueBuilder uniqueBuilder;
    private final UserDao userDao;
    private final LoginClient loginClient;
    private final TestConfig testConfig;

    @Guicy
    LoginControllerIT(UniqueData uniqueData,
            UniqueBuilder uniqueBuilder,
            UserDao userDao,
            LoginClient loginClient,
            TestConfig testConfig) {
        this.uniqueData = uniqueData;
        this.uniqueBuilder = uniqueBuilder;
        this.userDao = userDao;
        this.loginClient = loginClient;
        this.testConfig = testConfig;
    }

    @Test
    void loginWithEmail() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilder.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getEmail(), password);
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void loginWithHandle() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilder.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getHandle(), password);
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void loginSessionCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilder.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getEmail(), password);
        NewCookie cookie = response.getCookies().get(CookieNames.SESSION);
        assertThat(cookie.getDomain()).isEqualTo(this.testConfig.backendDomain());
        assertThat(cookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
        assertThat(cookie.getPath()).isEqualTo("/api/token");
        assertThat(cookie.isHttpOnly()).isTrue();
    }

    @Test
    void loginSessionLogoutCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilder.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getEmail(), password);
        NewCookie cookie = response.getCookies().get(CookieNames.SESSION_LOGOUT);
        assertThat(cookie.getDomain()).isEqualTo(this.testConfig.backendDomain());
        assertThat(cookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
        assertThat(cookie.getPath()).isEqualTo("/api/logout");
        assertThat(cookie.isHttpOnly()).isTrue();
    }

    @Test
    void loginUserIdCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilder.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getEmail(), password);
        NewCookie cookie = response.getCookies().get(CookieNames.USER_ID);
        assertThat(cookie.getDomain()).isEqualTo(this.testConfig.backendDomain());
        assertThat(cookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
        assertThat(cookie.getPath()).isEqualTo("/api/token");
        assertThat(cookie.isHttpOnly()).isTrue();
    }

    @Test
    void loginUserIdLogoutCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilder.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getEmail(), password);
        NewCookie cookie = response.getCookies().get(CookieNames.USER_ID_LOGOUT);
        assertThat(cookie.getDomain()).isEqualTo(this.testConfig.backendDomain());
        assertThat(cookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
        assertThat(cookie.getPath()).isEqualTo("/api/logout");
        assertThat(cookie.isHttpOnly()).isTrue();
    }

    @Test
    void loginBadIdentifier() {
        Response response = this.loginClient.login(this.uniqueData.email(), this.uniqueData.password());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void loginBadPassword() {
        User user = this.uniqueBuilder.user().build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getEmail(), this.uniqueData.password());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void loginNoPassword() {
        User user = this.uniqueBuilder.user().build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user.getEmail(), null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }
}
