package com.gitlab.akluball.lwb.itest.client;

import com.gitlab.akluball.lwb.itest.security.CookieNames;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

public class TokenClient {
    private final WebTarget target;

    @Inject
    TokenClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("token");
    }

    public Response getToken(Integer userId, String sessionKey) {
        Invocation.Builder invocationBuilder = this.target.request();
        if (Objects.nonNull(userId)) {
            invocationBuilder.cookie(CookieNames.USER_ID, userId.toString());
        }
        if (Objects.nonNull(sessionKey)) {
            invocationBuilder.cookie(CookieNames.SESSION, sessionKey);
        }
        return invocationBuilder.get();
    }
}
