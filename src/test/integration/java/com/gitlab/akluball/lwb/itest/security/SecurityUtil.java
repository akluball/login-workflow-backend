package com.gitlab.akluball.lwb.itest.security;

public class SecurityUtil {
    public static String bearer(String token) {
        return String.format("Bearer %s", token);
    }
}
