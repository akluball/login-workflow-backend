package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.RegistrationClient;
import com.gitlab.akluball.lwb.itest.data.RegistrationDao;
import com.gitlab.akluball.lwb.itest.data.UserDao;
import com.gitlab.akluball.lwb.itest.mailhog.MailHogClient;
import com.gitlab.akluball.lwb.itest.model.Registration;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.transfer.RegistrationDto;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import com.gitlab.akluball.lwb.itest.util.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class RegistrationControllerIT {
    private final UniqueBuilder uniqueBuilder;
    private final RegistrationClient registrationClient;
    private final RegistrationDao registrationDao;
    private UniqueData uniqueData;

    @Guicy
    RegistrationControllerIT(UniqueData uniqueData,
            UniqueBuilder uniqueBuilder,
            RegistrationClient registrationClient,
            RegistrationDao registrationDao) {
        this.uniqueData = uniqueData;
        this.uniqueBuilder = uniqueBuilder;
        this.registrationClient = registrationClient;
        this.registrationDao = registrationDao;
    }

    @Test
    void create() {
        String email = this.uniqueData.email();
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().email(email).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    @Guicy
    void createSendsVerificationToken(MailHogClient mailHogClient) {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().build();
        this.registrationClient.create(registrationDto);
        String sentCode = mailHogClient.getFirstEmailTo(registrationDto.getEmail()).getBody();
        String persistedCode = this.registrationDao.findByEmail(registrationDto.getEmail()).getVerificationCode();
        assertThat(sentCode).isEqualTo(persistedCode);
    }

    @Test
    void createNoEmail() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().email(null).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createBadEmail() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().email("not-an-email").build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoHandle() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().handle(null).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createEmptyHandle() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().handle("").build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoPassword() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().password(null).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createEmptyPassword() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().password("").build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoFirstName() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().firstName(null).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createEmptyFirstName() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().firstName("").build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoLastName() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().lastName(null).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createEmptyLastName() {
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().lastName("").build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createEmailConflictExistingRegistration() {
        String email = this.uniqueData.email();
        this.registrationDao.persist(this.uniqueBuilder.registration().email(email).build());
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().email(email).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(409);
        assertThat(response.readEntity(String.class)).containsMatch("conflict");
    }

    @Test
    @Guicy
    void createEmailConflictExistingUser(UserDao userDao) {
        String email = this.uniqueData.email();
        userDao.persist(this.uniqueBuilder.user().email(email).build());
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().email(email).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(409);
        assertThat(response.readEntity(String.class)).containsMatch("conflict");
    }

    @Test
    void createHandleConflictExistingRegistration() {
        String handle = this.uniqueData.handle();
        this.registrationDao.persist(this.uniqueBuilder.registration().handle(handle).build());
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().handle(handle).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(409);
        assertThat(response.readEntity(String.class)).containsMatch("conflict");
    }

    @Test
    @Guicy
    void createHandleConflictExistingUser(UserDao userDao) {
        String handle = this.uniqueData.handle();
        userDao.persist(this.uniqueBuilder.user().handle(handle).build());
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().handle(handle).build();
        Response response = this.registrationClient.create(registrationDto);
        assertThat(response.getStatus()).isEqualTo(409);
        assertThat(response.readEntity(String.class)).containsMatch("conflict");
    }

    @Test
    void verify() {
        Registration registration = this.uniqueBuilder.registration().build();
        this.registrationDao.persist(registration);
        Response response = this.registrationClient.verify(registration.getEmail(), registration.getVerificationCode());
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void verifyEmailNotFound() {
        Response response = this.registrationClient.verify(this.uniqueData.email(), this.uniqueData.verificationCode());
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void verifyBadCode() {
        Registration registration = this.uniqueBuilder.registration().build();
        this.registrationDao.persist(registration);
        Response response = this.registrationClient.verify(registration.getEmail(), this.uniqueData.verificationCode());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void verifyNoCode() {
        Registration registration = this.uniqueBuilder.registration().build();
        this.registrationDao.persist(registration);
        Response response = this.registrationClient.verify(registration.getEmail(), null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }
}
