package com.gitlab.akluball.lwb.itest.client;

import com.gitlab.akluball.lwb.itest.transfer.RegistrationDto;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

public class RegistrationClient {
    private final WebTarget target;

    @Inject
    RegistrationClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("registration");
    }

    public Response create(RegistrationDto registrationDto) {
        return this.target.request()
                .post(Entity.json(registrationDto));
    }

    public Response verify(String identifier, String verificationCode) {
        return this.target
                .path(identifier)
                .path("verification")
                .request()
                .post(Objects.nonNull(verificationCode) ? Entity.json(verificationCode) : null);
    }
}
