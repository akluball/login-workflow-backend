package com.gitlab.akluball.lwb.itest.transfer;

public class RegistrationDto {
    private String email;
    private String handle;
    private String password;
    private String firstName;
    private String lastName;

    public String getEmail() {
        return email;
    }

    public static class Builder {
        private final RegistrationDto registrationDto;

        Builder() {
            this.registrationDto = new RegistrationDto();
        }

        public Builder email(String email) {
            this.registrationDto.email = email;
            return this;
        }

        public Builder handle(String handle) {
            this.registrationDto.handle = handle;
            return this;
        }

        public Builder password(String password) {
            this.registrationDto.password = password;
            return this;
        }

        public Builder firstName(String firstName) {
            this.registrationDto.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.registrationDto.lastName = lastName;
            return this;
        }

        public RegistrationDto build() {
            return this.registrationDto;
        }
    }
}
