package com.gitlab.akluball.lwb.itest.hook;

import com.gitlab.akluball.lwb.itest.client.HealthCheckClient;
import com.gitlab.akluball.lwb.itest.mailhog.MailHogClient;
import com.gitlab.akluball.lwb.itest.param.ParameterResolverModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class BeforeSuite {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new ParameterResolverModule());
        injector.getInstance(MailHogClient.class).clear();
        EntityManager entityManager = injector.getInstance(EntityManager.class);
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.flush();
        tx.commit();
        entityManager.close();
        injector.getInstance(HealthCheckClient.class).healthCheck();
    }
}
