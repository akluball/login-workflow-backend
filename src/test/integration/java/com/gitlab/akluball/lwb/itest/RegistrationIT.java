package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.RegistrationClient;
import com.gitlab.akluball.lwb.itest.data.RegistrationDao;
import com.gitlab.akluball.lwb.itest.model.Registration;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.transfer.RegistrationDto;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import com.gitlab.akluball.lwb.itest.util.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class RegistrationIT {
    private final UniqueBuilder uniqueBuilder;
    private final RegistrationClient registrationClient;
    private final RegistrationDao registrationDao;
    private UniqueData uniqueData;

    @Guicy
    RegistrationIT(UniqueData uniqueData,
            UniqueBuilder uniqueBuilder,
            RegistrationClient registrationClient,
            RegistrationDao registrationDao) {
        this.uniqueData = uniqueData;
        this.uniqueBuilder = uniqueBuilder;
        this.registrationClient = registrationClient;
        this.registrationDao = registrationDao;
    }

    @Test
    void createPersistsEmail() {
        String email = this.uniqueData.email();
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().email(email).build();
        this.registrationClient.create(registrationDto);
        assertThat(this.registrationDao.findByEmail(email)).isNotNull();
    }

    @Test
    void createPersistsHandle() {
        String handle = this.uniqueData.handle();
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().handle(handle).build();
        this.registrationClient.create(registrationDto);
        Registration registration = this.registrationDao.findByEmail(registrationDto.getEmail());
        assertThat(registration.getHandle()).isEqualTo(handle);
    }

    @Test
    void createPersistsPassword() {
        String password = this.uniqueData.password();
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().password(password).build();
        this.registrationClient.create(registrationDto);
        Registration persisted = this.registrationDao.findByEmail(registrationDto.getEmail());
        assertThat(persisted.isPassword(password)).isTrue();
    }

    @Test
    void createPersistsName() {
        String firstName = this.uniqueData.firstName();
        String lastName = this.uniqueData.lastName();
        RegistrationDto registrationDto = this.uniqueBuilder.registrationDto().firstName(firstName).lastName(lastName)
                .build();
        this.registrationClient.create(registrationDto);
        Registration persisted = this.registrationDao.findByEmail(registrationDto.getEmail());
        assertThat(persisted.getFirstName()).isEqualTo(firstName);
        assertThat(persisted.getLastName()).isEqualTo(lastName);
    }

    @Test
    void verifyDeletesRegistration() {
        String email = this.uniqueData.email();
        String verificationCode = this.uniqueData.verificationCode();
        Registration registration = this.uniqueBuilder.registration().email(email).verificationCode(verificationCode)
                .build();
        this.registrationDao.persist(registration);
        this.registrationClient.verify(email, verificationCode);
        this.registrationDao.detach(registration);
        assertThat(this.registrationDao.findByEmail(email)).isNull();
    }
}
