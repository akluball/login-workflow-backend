package com.gitlab.akluball.lwb.itest.mailhog;

import javax.json.bind.annotation.JsonbProperty;

public class MailHogEmail {
    @JsonbProperty("Content")
    private Content content;

    public String getBody() {
        return this.content.body;
    }

    public static class Content {
        @JsonbProperty("Body")
        private String body;
    }
}
