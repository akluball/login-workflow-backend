package com.gitlab.akluball.lwb.itest.data;

import com.gitlab.akluball.lwb.itest.model.UniqueDataIdWrapper;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class UniqueDataDao {
    private final EntityManager entityManager;

    @Inject
    UniqueDataDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public int nextId() {
        UniqueDataIdWrapper wrapper = new UniqueDataIdWrapper();
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.persist(wrapper);
        tx.commit();
        return wrapper.getId();
    }
}
