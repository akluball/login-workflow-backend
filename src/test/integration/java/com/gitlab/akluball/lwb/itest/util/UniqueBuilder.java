package com.gitlab.akluball.lwb.itest.util;

import com.gitlab.akluball.lwb.itest.model.Registration;
import com.gitlab.akluball.lwb.itest.model.Session;
import com.gitlab.akluball.lwb.itest.model.User;
import com.gitlab.akluball.lwb.itest.transfer.RegistrationDto;

import javax.inject.Inject;
import javax.inject.Provider;
import java.time.Instant;

public class UniqueBuilder {
    private final UniqueData uniqueData;
    private final Provider<RegistrationDto.Builder> registrationDtoBuilderProvider;
    private final Provider<Registration.Builder> registrationBuilderProvider;
    private final Provider<User.Builder> userBuilderProvider;
    private final Provider<Session.Builder> sessionBuilderProvider;

    @Inject
    UniqueBuilder(UniqueData uniqueData,
            Provider<RegistrationDto.Builder> registrationDtoBuilderProvider,
            Provider<Registration.Builder> registrationBuilderProvider,
            Provider<User.Builder> userBuilderProvider,
            Provider<Session.Builder> sessionBuilderProvider) {
        this.uniqueData = uniqueData;
        this.registrationDtoBuilderProvider = registrationDtoBuilderProvider;
        this.registrationBuilderProvider = registrationBuilderProvider;
        this.userBuilderProvider = userBuilderProvider;
        this.sessionBuilderProvider = sessionBuilderProvider;
    }

    public RegistrationDto.Builder registrationDto() {
        return registrationDtoBuilderProvider.get()
                .email(this.uniqueData.email())
                .handle(this.uniqueData.handle())
                .password(this.uniqueData.password())
                .firstName(this.uniqueData.firstName())
                .lastName(this.uniqueData.lastName());
    }

    public Registration.Builder registration() {
        return registrationBuilderProvider.get()
                .email(this.uniqueData.email())
                .handle(this.uniqueData.handle())
                .password(this.uniqueData.password())
                .firstName(this.uniqueData.firstName())
                .lastName(this.uniqueData.lastName())
                .verificationCode(this.uniqueData.verificationCode());
    }

    public User.Builder user() {
        return this.userBuilderProvider.get()
                .email(this.uniqueData.email())
                .handle(this.uniqueData.handle())
                .password(this.uniqueData.password())
                .firstName(this.uniqueData.firstName())
                .lastName(this.uniqueData.lastName());
    }

    public Session.Builder session() {
        return this.sessionBuilderProvider.get()
                .sessionKey(this.uniqueData.sessionKey())
                .expiration(Instant.now().plusSeconds(3600));
    }
}
