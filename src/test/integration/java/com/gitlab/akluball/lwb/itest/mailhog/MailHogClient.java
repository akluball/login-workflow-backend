package com.gitlab.akluball.lwb.itest.mailhog;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static com.google.common.truth.Truth.assertThat;

@Singleton
public class MailHogClient {
    private static final String MAILHOG_URI = System.getenv("MAILHOG_URI");
    private final Client client;
    private final Jsonb jsonb;

    @Inject
    MailHogClient(Client client, Jsonb jsonb) {
        this.client = client;
        this.jsonb = jsonb;
    }

    public void clear() {
        URI deleteMessagesUri = UriBuilder.fromUri(MAILHOG_URI)
                .path("api")
                .path("v1")
                .path("messages")
                .build();
        Response response = this.client.target(deleteMessagesUri)
                .request()
                .delete();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    public MailHogEmail getFirstEmailTo(String recipient) {
        URI emailSearchUri = UriBuilder.fromUri(MAILHOG_URI)
                .path("api")
                .path("v2")
                .path("search")
                .queryParam("kind", "to")
                .queryParam("query", recipient)
                .build();
        Response response = this.client.target(emailSearchUri)
                .request()
                .get();
        String json = response.readEntity(String.class);
        MailHogSearchResult mailHogSearchResult = this.jsonb
                .fromJson(json, MailHogSearchResult.class);
        assertThat(mailHogSearchResult.isNonEmptyResult()).isTrue();
        return mailHogSearchResult.getFirstResult();
    }
}
