package com.gitlab.akluball.lwb.itest.util;

import com.google.common.collect.Range;

import java.time.Instant;

public class TestRanges {
    public static Range<Instant> bufferBelow5Seconds(Instant upperBound) {
        return Range.closed(upperBound.minusSeconds(5), upperBound);
    }

    public static Range<Long> bufferBelow5000(long upperBound) {
        return Range.closed(upperBound - 5000, upperBound);
    }
}
