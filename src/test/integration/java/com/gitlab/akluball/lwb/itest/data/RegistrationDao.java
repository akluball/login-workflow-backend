package com.gitlab.akluball.lwb.itest.data;

import com.gitlab.akluball.lwb.itest.model.Registration;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class RegistrationDao {
    private final EntityManager entityManager;

    @Inject
    RegistrationDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(Registration registration) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.persist(registration);
        tx.commit();
    }

    public Registration findByEmail(String email) {
        return this.entityManager.find(Registration.class, email);
    }

    public void detach(Registration registration) {
        this.entityManager.detach(registration);
    }
}
