package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.HealthCheckClient;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class HealthCheckControllerIT {
    private final HealthCheckClient healthCheckClient;

    @Guicy
    public HealthCheckControllerIT(HealthCheckClient healthCheckClient) {
        this.healthCheckClient = healthCheckClient;
    }

    @Test
    public void someTest() {
        Response response = this.healthCheckClient.healthCheck();
        assertThat(response.getStatus()).isEqualTo(204);
    }
}
