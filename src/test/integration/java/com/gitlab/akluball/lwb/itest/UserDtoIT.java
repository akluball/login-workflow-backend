package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.UserClient;
import com.gitlab.akluball.lwb.itest.data.UserDao;
import com.gitlab.akluball.lwb.itest.model.User;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.security.UserSecurity;
import com.gitlab.akluball.lwb.itest.transfer.UserDto;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import com.gitlab.akluball.lwb.itest.util.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class UserDtoIT {
    private final UniqueData uniqueData;
    private final UniqueBuilder uniqueBuilder;
    private final UserDao userDao;
    private final UserSecurity userSecurity;
    private final UserClient userClient;

    @Guicy
    UserDtoIT(UniqueData uniqueData,
            UniqueBuilder uniqueBuilder,
            UserDao userDao,
            UserSecurity userSecurity,
            UserClient userClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilder = uniqueBuilder;
        this.userDao = userDao;
        this.userSecurity = userSecurity;
        this.userClient = userClient;
    }

    @Test
    void id() {
        User user = this.uniqueBuilder.user().build();
        this.userDao.persist(user);
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.readEntity(UserDto.class).getId()).isEqualTo(user.getId());
    }

    @Test
    void handle() {
        String handle = this.uniqueData.handle();
        User user = this.uniqueBuilder.user().handle(handle).build();
        this.userDao.persist(user);
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.readEntity(UserDto.class).getHandle()).isEqualTo(user.getHandle());
    }

    @Test
    void email() {
        String email = this.uniqueData.email();
        User user = this.uniqueBuilder.user().email(email).build();
        this.userDao.persist(user);
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.readEntity(UserDto.class).getEmail()).isEqualTo(user.getEmail());
    }

    @Test
    void firstName() {
        String firstName = this.uniqueData.firstName();
        User user = this.uniqueBuilder.user().firstName(firstName).build();
        this.userDao.persist(user);
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.readEntity(UserDto.class).getFirstName()).isEqualTo(user.getFirstName());
    }

    @Test
    void lastName() {
        String lastName = this.uniqueData.lastName();
        User user = this.uniqueBuilder.user().lastName(lastName).build();
        this.userDao.persist(user);
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.readEntity(UserDto.class).getLastName()).isEqualTo(user.getLastName());
    }

    @Test
    void joinEpochSeconds() {
        User user = this.uniqueBuilder.user().joinEpochSeconds(100L).build();
        this.userDao.persist(user);
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.readEntity(UserDto.class).getJoinEpochSeconds()).isEqualTo(100L);
    }
}
