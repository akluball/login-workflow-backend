package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.UserClient;
import com.gitlab.akluball.lwb.itest.data.UserDao;
import com.gitlab.akluball.lwb.itest.model.User;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.security.UserSecurity;
import com.gitlab.akluball.lwb.itest.transfer.UserDto;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class UserControllerIT {
    private final UniqueBuilder uniqueBuilder;
    private final UserDao userDao;
    private final UserSecurity userSecurity;
    private final UserClient userClient;

    @Guicy
    UserControllerIT(UniqueBuilder uniqueBuilder,
            UserDao userDao,
            UserSecurity userSecurity,
            UserClient userClient) {
        this.uniqueBuilder = uniqueBuilder;
        this.userDao = userDao;
        this.userSecurity = userSecurity;
        this.userClient = userClient;
    }

    private User persistedUser() {
        User user = this.uniqueBuilder.user().build();
        this.userDao.persist(user);
        return user;
    }

    @Test
    void currentUser() {
        User user = persistedUser();
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getId()).isEqualTo(user.getId());
    }

    @Test
    void currentUserDeleted() {
        User user = persistedUser();
        this.userDao.delete(user);
        Response response = this.userClient.currentUser(this.userSecurity.tokenFor(user));
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).contains("not found");
    }

    @Test
    void currentUserNoAuth() {
        Response response = this.userClient.currentUser(null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("bearer authorization required");
    }

    @Test
    @Guicy
    void currentUserBadTokenSignature(UserSecurity userSecurity) {
        User user = persistedUser();
        String badSignatureToken = userSecurity.badSignatureTokenFor(user);
        Response response = this.userClient.currentUser(badSignatureToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid bearer token");
    }
}
