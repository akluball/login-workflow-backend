package com.gitlab.akluball.lwb.itest.model;

import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "LoginWorkflowUser")
@Access(AccessType.FIELD)
public class User {
    @Id
    @GeneratedValue
    private int id;
    private String email;
    private String handle;
    private String passwordHash;
    private String salt;
    private String firstName;
    private String lastName;
    private long joinEpochSeconds;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Session> sessions;

    public User() {
        this.sessions = new HashSet<>();
    }

    public int getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    public String getHandle() {
        return handle;
    }

    public boolean isPassword(String password) {
        return this.passwordHash.equals(BCrypt.hashpw(password, this.salt));
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getJoinEpochSeconds() {
        return joinEpochSeconds;
    }

    public Set<Session> getSessions() {
        return sessions;
    }

    public Session getSessionFor(String sessionKey) {
        return this.sessions.stream()
                .filter(session -> session.isSessionKey(sessionKey))
                .findFirst()
                .orElse(null);
    }

    public String getAnySessionKey() {
        return this.sessions.iterator().next().getSessionKey();
    }

    public static class Builder {
        private final User user;

        public Builder() {
            this.user = new User();
        }

        public Builder email(String email) {
            this.user.email = email;
            return this;
        }

        public Builder handle(String handle) {
            this.user.handle = handle;
            return this;
        }

        public Builder password(String password) {
            String salt = BCrypt.gensalt();
            this.user.passwordHash = BCrypt.hashpw(password, salt);
            this.user.salt = salt;
            return this;
        }

        public Builder firstName(String firstName) {
            this.user.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.user.lastName = lastName;
            return this;
        }

        public Builder joinEpochSeconds(long joinEpochSeconds) {
            this.user.joinEpochSeconds = joinEpochSeconds;
            return this;
        }

        public Builder session(Session session) {
            this.user.sessions.add(session);
            return this;
        }

        public User build() {
            return this.user;
        }
    }
}
