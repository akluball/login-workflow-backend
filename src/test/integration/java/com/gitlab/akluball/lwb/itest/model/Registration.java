package com.gitlab.akluball.lwb.itest.model;

import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Access(AccessType.FIELD)
public class Registration {
    @Id
    private String email;
    private String handle;
    private String salt;
    private String passwordHash;
    private String firstName;
    private String lastName;
    private String verificationCode;

    public String getEmail() {
        return this.email;
    }

    public String getHandle() {
        return handle;
    }

    public boolean isPassword(String password) {
        return this.passwordHash.equals(BCrypt.hashpw(password, this.salt));
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public static class Builder {
        private final Registration registration;

        public Builder() {
            this.registration = new Registration();
        }

        public Builder email(String email) {
            this.registration.email = email;
            return this;
        }

        public Builder handle(String handle) {
            this.registration.handle = handle;
            return this;
        }

        public Builder verificationCode(String verificationCode) {
            this.registration.verificationCode = verificationCode;
            return this;
        }

        public Builder password(String password) {
            String salt = BCrypt.gensalt();
            this.registration.salt = salt;
            this.registration.passwordHash = BCrypt.hashpw(password, salt);
            return this;
        }

        public Builder firstName(String firstName) {
            this.registration.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.registration.lastName = lastName;
            return this;
        }

        public Registration build() {
            return this.registration;
        }
    }
}
