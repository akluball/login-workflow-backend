package com.gitlab.akluball.lwb.itest.security;

import com.gitlab.akluball.lwb.itest.TestConfig;
import com.gitlab.akluball.lwb.itest.model.User;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.security.PrivateKey;
import java.time.Instant;
import java.util.Date;

@Singleton
public class UserSecurity {
    private final PrivateKey privateKey;
    private final PrivateKey badPrivateKey;
    private final JwtParser parser;

    @Inject
    UserSecurity(TestConfig testConfig) {
        this.privateKey = testConfig.privateKey();
        this.parser = Jwts.parser().setSigningKey(testConfig.publicKey());
        this.badPrivateKey = Keys.keyPairFor(SignatureAlgorithm.RS256).getPrivate();
    }

    public boolean isValidSignature(String serializedToken) {
        try {
            this.parser.parseClaimsJws(serializedToken);
            return true;
        } catch (SignatureException | ExpiredJwtException e) {
            return false;
        }
    }

    public String getSubject(String serializedToken) {
        return this.parser.parseClaimsJws(serializedToken)
                .getBody()
                .getSubject();
    }

    public Date getExpiration(String serializedToken) {
        return this.parser.parseClaimsJws(serializedToken)
                .getBody()
                .getExpiration();
    }

    public String tokenFor(User user) {
        return Jwts.builder()
                .signWith(this.privateKey)
                .setSubject("" + user.getId())
                .setExpiration(Date.from(Instant.now().plusSeconds(60)))
                .compact();
    }

    public String badSignatureTokenFor(User user) {
        return Jwts.builder()
                .signWith(this.badPrivateKey)
                .setSubject("" + user.getId())
                .setExpiration(Date.from(Instant.now().plusSeconds(60)))
                .compact();
    }
}
