package com.gitlab.akluball.lwb.itest.client;

import com.gitlab.akluball.lwb.itest.TestConfig;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

public class ApiWrapper {
    private final WebTarget target;

    @Inject
    ApiWrapper(TestConfig testConfig, Client client) {
        this.target = client.target(testConfig.backendUri())
                .path("api");
    }

    public WebTarget target() {
        return this.target;
    }
}
