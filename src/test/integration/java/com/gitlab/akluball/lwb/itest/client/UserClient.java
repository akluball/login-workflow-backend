package com.gitlab.akluball.lwb.itest.client;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

import static com.gitlab.akluball.lwb.itest.security.SecurityUtil.bearer;

public class UserClient {
    private final WebTarget target;

    @Inject
    UserClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("user");
    }

    public Response currentUser(String token) {
        Invocation.Builder invocationBuilder = this.target.request();
        if (Objects.nonNull(token)) {
            invocationBuilder.header("Authorization", bearer(token));
        }
        return invocationBuilder.get();
    }
}
