package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.TokenClient;
import com.gitlab.akluball.lwb.itest.data.UserDao;
import com.gitlab.akluball.lwb.itest.model.Session;
import com.gitlab.akluball.lwb.itest.model.User;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.security.UserSecurity;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.time.Instant;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class TokenControllerIT {
    private final UniqueBuilder uniqueBuilder;
    private final UserDao userDao;
    private final TokenClient tokenClient;

    @Inject
    @Guicy
    TokenControllerIT(UniqueBuilder uniqueBuilder, UserDao userDao, TokenClient tokenClient) {
        this.uniqueBuilder = uniqueBuilder;
        this.userDao = userDao;
        this.tokenClient = tokenClient;
    }

    private User userWithSession() {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().session(session).build();
        this.userDao.persist(user);
        return user;
    }

    @Test
    void token() {
        User user = userWithSession();
        Response response = this.tokenClient.getToken(user.getId(), user.getAnySessionKey());
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    @Guicy
    void tokenSigned(UserSecurity userSecurity) {
        User user = userWithSession();
        String serializedToken = this.tokenClient.getToken(user.getId(), user.getAnySessionKey())
                .readEntity(String.class);
        assertThat(userSecurity.isValidSignature(serializedToken)).isTrue();
    }

    @Test
    @Guicy
    void tokenSubject(UserSecurity userSecurity) {
        User user = userWithSession();
        String serializedToken = this.tokenClient.getToken(user.getId(), user.getAnySessionKey())
                .readEntity(String.class);
        assertThat(userSecurity.getSubject(serializedToken)).isEqualTo("" + user.getId());
    }

    @Test
    void tokenNoUserIdCookie() {
        User user = userWithSession();
        Response response = this.tokenClient.getToken(null, user.getAnySessionKey());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void tokenNoSessionCookie() {
        User user = userWithSession();
        Response response = this.tokenClient.getToken(user.getId(), null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void tokenUserNotFound() {
        User user = userWithSession();
        userDao.delete(user);
        Response response = this.tokenClient.getToken(user.getId(), user.getAnySessionKey());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void tokenBadSessionKey() {
        User user = userWithSession();
        Response response = this.tokenClient.getToken(user.getId(), String.format("bad%s", user.getAnySessionKey()));
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    @Guicy
    void tokenTimedOutSession(UserDao userDao) {
        Session session = this.uniqueBuilder.session().expiration(Instant.now().minusSeconds(5)).build();
        User user = this.uniqueBuilder.user().session(session).build();
        userDao.persist(user);
        Response response = this.tokenClient.getToken(user.getId(), session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }
}
