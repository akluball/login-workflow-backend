package com.gitlab.akluball.lwb.itest.param;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;

import javax.inject.Singleton;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParameterResolverModule implements Module {
    private final ThreadLocal<EntityManager> emThreadLocal;
    private final ThreadLocal<EntityManagerFactory> emfThreadLocal;
    private final ThreadLocal<Client> clientThreadLocal;

    public ParameterResolverModule() {
        this.emfThreadLocal = new ThreadLocal<EntityManagerFactory>();
        this.emThreadLocal = new ThreadLocal<EntityManager>();
        this.clientThreadLocal = new ThreadLocal<Client>();
    }

    @Override
    public void configure(Binder binder) {
    }

    @Provides
    EntityManagerFactory entityManagerFactory() {
        if (this.emfThreadLocal.get() == null) {
            Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
            Map<String, String> props = new HashMap<>();
            props.put("javax.persistence.schema-generation.database.action", System.getenv("SCHEMA_GENERATION_ACTION"));
            props.put("javax.persistence.jdbc.driver", "org.postgresql.Driver");
            props.put("javax.persistence.jdbc.url", System.getenv("POSTGRES_URL"));
            props.put("javax.persistence.jdbc.user", System.getenv("POSTGRES_USER"));
            props.put("javax.persistence.jdbc.password", System.getenv("POSTGRES_PASSWORD"));
            props.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
            // uncomment below to see sql statements for debugging
            // props.put("hibernate.show_sql", "true");
            this.emfThreadLocal.set(Persistence.createEntityManagerFactory("LWB_IT", props));
        }
        return this.emfThreadLocal.get();
    }

    @Provides
    EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
        if (this.emThreadLocal.get() == null) {
            this.emThreadLocal.set(entityManagerFactory.createEntityManager());
        }
        return this.emThreadLocal.get();
    }

    @Provides
    @Singleton
    JsonbConfig jsonbConfig() {
        return new JsonbConfig()
                .withPropertyVisibilityStrategy(new FieldVisibilityStrategy());
    }

    @Provides
    @Singleton
    Jsonb jsonb(JsonbConfig jsonbConfig) {
        return JsonbBuilder.create(jsonbConfig);
    }

    @Provides
    Client client(JsonbMessageBodyReader reader, JsonbMessageBodyWriter writer) {
        if (this.clientThreadLocal.get() == null) {
            this.clientThreadLocal.set(ClientBuilder.newClient()
                    .register(reader)
                    .register(writer));
        }
        return this.clientThreadLocal.get();
    }
}
