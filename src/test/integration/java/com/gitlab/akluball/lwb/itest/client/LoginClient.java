package com.gitlab.akluball.lwb.itest.client;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

public class LoginClient {
    private final WebTarget target;

    @Inject
    LoginClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("login");
    }

    public Response login(String identifier, String password) {
        return this.target.path(identifier)
                .request()
                .post(Objects.nonNull(password) ? Entity.json(password) : null);
    }
}
