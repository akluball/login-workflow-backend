package com.gitlab.akluball.lwb.itest.mailhog;

import java.util.List;

public class MailHogSearchResult {
    private int total;
    private List<MailHogEmail> items;

    public boolean isNonEmptyResult() {
        return this.total > 0;
    }

    public MailHogEmail getFirstResult() {
        return this.items.get(0);
    }
}
