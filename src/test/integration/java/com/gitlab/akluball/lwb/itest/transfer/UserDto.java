package com.gitlab.akluball.lwb.itest.transfer;

public class UserDto {
    private int id;
    private String handle;
    private String email;
    private String firstName;
    private String lastName;
    private long joinEpochSeconds;

    public int getId() {
        return id;
    }

    public String getHandle() {
        return handle;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getJoinEpochSeconds() {
        return joinEpochSeconds;
    }
}
