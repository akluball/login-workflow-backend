package com.gitlab.akluball.lwb.itest.client;

import javax.inject.Inject;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class HealthCheckClient {
    private final WebTarget target;

    @Inject
    HealthCheckClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("healthcheck");
    }

    public Response healthCheck() {
        return this.target.request().get();
    }
}
