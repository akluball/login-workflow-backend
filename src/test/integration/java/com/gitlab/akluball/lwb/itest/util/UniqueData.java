package com.gitlab.akluball.lwb.itest.util;

import com.gitlab.akluball.lwb.itest.data.UniqueDataDao;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class UniqueData {
    private static final String EMAIL_TEMPLATE = "user-%s-%s@example.com";
    private static final String HANDLE_TEMPLATE = "%shandle%s";
    private static final String PASSWORD_TEMPLATE = "pass%sID%s";
    private static final String FIRST_NAME_TEMPLATE = "firstName%sID%s";
    private static final String LAST_NAME_TEMPLATE = "lastName%sID%s";
    private static final String VERIFICATION_CODE_TEMPLATE = "xty3f%sID%05d";
    private static final String SESSION_KEY_TEMPLATE = "kr34x%sID%05d";

    // id makes data unique across threads
    private final int id;
    private AtomicInteger emailCounter = new AtomicInteger();
    private AtomicInteger handleCounter = new AtomicInteger();
    private AtomicInteger passwordCounter = new AtomicInteger();
    private AtomicInteger firstNameCounter = new AtomicInteger();
    private AtomicInteger lastNameCounter = new AtomicInteger();
    private AtomicInteger verificationCodeCounter = new AtomicInteger();
    private AtomicInteger sessionKeyCounter = new AtomicInteger();

    @Inject
    UniqueData(UniqueDataDao uniqueDataDao) {
        this.id = uniqueDataDao.nextId();
    }

    private String generate(String template, AtomicInteger counter) {
        return String.format(template, this.id, counter.getAndIncrement());
    }

    public String email() {
        return this.generate(EMAIL_TEMPLATE, this.emailCounter);
    }

    public String handle() {
        return this.generate(HANDLE_TEMPLATE, this.handleCounter);
    }

    public String password() {
        return this.generate(PASSWORD_TEMPLATE, this.passwordCounter);
    }

    public String firstName() {
        return this.generate(FIRST_NAME_TEMPLATE, this.firstNameCounter);
    }

    public String lastName() {
        return this.generate(LAST_NAME_TEMPLATE, this.lastNameCounter);
    }

    public String verificationCode() {
        return this.generate(VERIFICATION_CODE_TEMPLATE, verificationCodeCounter);
    }

    public String sessionKey() {
        return this.generate(SESSION_KEY_TEMPLATE, this.sessionKeyCounter);
    }
}
