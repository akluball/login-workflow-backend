package com.gitlab.akluball.lwb.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Access(AccessType.FIELD)
public class Session {
    @Id
    @GeneratedValue
    private int id;
    private String sessionKey;
    private Timestamp expiration;
    private Timestamp freshestTokenExpiration;

    public String getSessionKey() {
        return this.sessionKey;
    }

    public boolean isSessionKey(String toCheck) {
        return this.sessionKey.equals(toCheck);
    }

    public Timestamp getExpiration() {
        return this.expiration;
    }

    public Timestamp getFreshestTokenExpiration() {
        return this.freshestTokenExpiration;
    }

    public static class Builder {
        private final Session session;

        public Builder() {
            this.session = new Session();
        }

        public Builder sessionKey(String sessionKey) {
            this.session.sessionKey = sessionKey;
            return this;
        }

        public Builder expiration(Instant expirationInstant) {
            this.session.expiration = Timestamp.from(expirationInstant);
            return this;
        }

        public Session build() {
            return this.session;
        }
    }
}
