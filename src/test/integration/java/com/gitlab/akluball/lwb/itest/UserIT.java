package com.gitlab.akluball.lwb.itest;

import com.gitlab.akluball.lwb.itest.client.LoginClient;
import com.gitlab.akluball.lwb.itest.client.LogoutClient;
import com.gitlab.akluball.lwb.itest.client.RegistrationClient;
import com.gitlab.akluball.lwb.itest.client.TokenClient;
import com.gitlab.akluball.lwb.itest.data.RegistrationDao;
import com.gitlab.akluball.lwb.itest.data.UserDao;
import com.gitlab.akluball.lwb.itest.model.Registration;
import com.gitlab.akluball.lwb.itest.model.Session;
import com.gitlab.akluball.lwb.itest.model.User;
import com.gitlab.akluball.lwb.itest.param.Guicy;
import com.gitlab.akluball.lwb.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.lwb.itest.security.CookieNames;
import com.gitlab.akluball.lwb.itest.util.UniqueBuilder;
import com.gitlab.akluball.lwb.itest.util.UniqueData;
import com.google.common.collect.Range;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.time.Instant;

import static com.gitlab.akluball.lwb.itest.util.TestRanges.bufferBelow5000;
import static com.gitlab.akluball.lwb.itest.util.TestRanges.bufferBelow5Seconds;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class UserIT {
    private final UniqueBuilder uniqueBuilder;
    private final RegistrationDao registrationDao;
    private final UserDao userDao;
    private final RegistrationClient registrationClient;
    private UniqueData uniqueData;

    @Guicy
    UserIT(UniqueData uniqueData,
            UniqueBuilder uniqueBuilder,
            RegistrationDao registrationDao,
            UserDao userDao,
            RegistrationClient registrationClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilder = uniqueBuilder;
        this.registrationDao = registrationDao;
        this.userDao = userDao;
        this.registrationClient = registrationClient;
    }

    @Test
    void verifyCreatesUserSetsEmail() {
        String email = this.uniqueData.email();
        Registration registration = this.uniqueBuilder.registration().email(email).build();
        this.registrationDao.persist(registration);
        this.registrationClient.verify(email, registration.getVerificationCode());
        assertThat(this.userDao.findByEmail(email)).isNotNull();
    }

    @Test
    void verifyCreatesUserSetsHandle() {
        String email = this.uniqueData.email();
        String handle = this.uniqueData.handle();
        Registration registration = this.uniqueBuilder.registration().email(email).handle(handle).build();
        this.registrationDao.persist(registration);
        this.registrationClient.verify(email, registration.getVerificationCode());
        assertThat(this.userDao.findByEmail(email).getHandle()).isEqualTo(handle);
    }

    @Test
    void verifyCreatesUserSetsPassword() {
        String email = this.uniqueData.email();
        String password = this.uniqueData.password();
        Registration registration = this.uniqueBuilder.registration().email(email).password(password).build();
        this.registrationDao.persist(registration);
        this.registrationClient.verify(email, registration.getVerificationCode());
        assertThat(this.userDao.findByEmail(email).isPassword(password)).isTrue();
    }

    @Test
    void verifyCreatesUserSetsFirstName() {
        String email = this.uniqueData.email();
        String firstName = this.uniqueData.firstName();
        Registration registration = this.uniqueBuilder.registration().email(email).firstName(firstName).build();
        this.registrationDao.persist(registration);
        this.registrationClient.verify(email, registration.getVerificationCode());
        assertThat(this.userDao.findByEmail(email).getFirstName()).isEqualTo(firstName);
    }

    @Test
    void verifyCreatesUserSetsLastName() {
        String email = this.uniqueData.email();
        String lastName = this.uniqueData.lastName();
        Registration registration = this.uniqueBuilder.registration().email(email).lastName(lastName).build();
        this.registrationDao.persist(registration);
        this.registrationClient.verify(email, registration.getVerificationCode());
        assertThat(this.userDao.findByEmail(email).getLastName()).isEqualTo(lastName);
    }

    @Test
    void verifyCreatesUserSetsJoinEpochSeconds() {
        String email = this.uniqueData.email();
        Registration registration = this.uniqueBuilder.registration().email(email).build();
        this.registrationDao.persist(registration);
        this.registrationClient.verify(email, registration.getVerificationCode());
        User user = this.userDao.findByEmail(email);
        assertThat(user.getJoinEpochSeconds()).isIn(bufferBelow5000(Instant.now().getEpochSecond()));
    }

    @Test
    @Guicy
    void loginAddsSessionToUser(LoginClient loginClient, TestConfig testConfig) {
        String handle = this.uniqueData.handle();
        String password = this.uniqueData.password();
        User user = this.uniqueBuilder.user().handle(handle).password(password).build();
        userDao.persist(user);
        Response response = loginClient.login(handle, password);
        NewCookie cookie = response.getCookies().get(CookieNames.SESSION);
        userDao.refresh(user);
        Session persistedSession = user.getSessionFor(cookie.getValue());
        Range<Instant> expirationRange
                = bufferBelow5Seconds(Instant.now().plusSeconds(testConfig.sessionLifeSeconds()));
        assertThat(persistedSession.getExpiration().toInstant()).isIn(expirationRange);
    }

    @Test
    @Guicy
    void getTokenUpdatesSessionsFreshestTokenExpiration(TokenClient tokenClient, TestConfig testConfig) {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().session(session).build();
        this.userDao.persist(user);
        tokenClient.getToken(user.getId(), user.getAnySessionKey()).readEntity(String.class);
        Range<Instant> expectedExpiration
                = bufferBelow5Seconds(Instant.now().plusSeconds(testConfig.tokenLifeSeconds()));
        this.userDao.refresh(user);
        assertThat(session.getFreshestTokenExpiration().toInstant()).isIn(expectedExpiration);
    }

    @Test
    @Guicy
    void logoutDeletesSession(LogoutClient logoutClient) {
        Session session = this.uniqueBuilder.session().build();
        User user = this.uniqueBuilder.user().session(session).build();
        this.userDao.persist(user);
        logoutClient.logout(user.getId(), session.getSessionKey());
        this.userDao.refresh(user);
        assertThat(user.getSessions()).doesNotContain(session);
    }
}
