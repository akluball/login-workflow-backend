package com.gitlab.akluball.lwb.itest.data;

import com.gitlab.akluball.lwb.itest.model.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class UserDao {
    private final EntityManager entityManager;

    @Inject
    UserDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(User user) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.persist(user);
        tx.commit();
    }

    public void refresh(User user) {
        this.entityManager.refresh(user);
    }

    public User findByEmail(String email) {
        String query = String.format("SELECT user from User user WHERE user.email = '%s'", email);
        return this.entityManager.createQuery(query, User.class).getSingleResult();
    }

    public void delete(User user) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.remove(user);
        tx.commit();
    }
}
