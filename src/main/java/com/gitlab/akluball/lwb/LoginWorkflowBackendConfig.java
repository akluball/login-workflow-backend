package com.gitlab.akluball.lwb;

import javax.inject.Singleton;
import javax.mail.Session;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Properties;

@Singleton
public class LoginWorkflowBackendConfig {
    private final int sessionLifeSeconds;
    private final int tokenLifeSeconds;
    private final PrivateKey privateKey;
    private final PublicKey publicKey;
    private final Session mailSession;
    private final String mailFrom;

    LoginWorkflowBackendConfig() throws NoSuchAlgorithmException, InvalidKeySpecException {
        this.sessionLifeSeconds = Integer.parseInt(System.getenv("SESSION_LIFE_SECONDS"));
        this.tokenLifeSeconds = Integer.parseInt(System.getenv("TOKEN_LIFE_SECONDS"));
        String privateKeyString = System.getenv("PRIVATE_KEY")
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "")
                .replaceAll("\\s", "");
        byte[] privateKeyBytes = Base64.getDecoder().decode(privateKeyString);
        this.privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        String publicKeyString = System.getenv("PUBLIC_KEY")
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "")
                .replaceAll("\\s", "");
        byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyString);
        this.publicKey = KeyFactory.getInstance("RSA")
                .generatePublic(new X509EncodedKeySpec(publicKeyBytes));
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.host", System.getenv("SMTP_HOST"));
        mailProperties.put("mail.smtp.port", System.getenv("SMTP_PORT"));
        this.mailSession = Session.getInstance(mailProperties);
        this.mailFrom = System.getenv("SMTP_FROM");
    }

    public int sessionLifeSeconds() {
        return this.sessionLifeSeconds;
    }

    public int tokenLifeSeconds() {
        return this.tokenLifeSeconds;
    }

    public PrivateKey privateKey() {
        return this.privateKey;
    }

    public PublicKey publicKey() {
        return this.publicKey;
    }

    public Session mailSession() {
        return this.mailSession;
    }

    public String mailFrom() {
        return this.mailFrom;
    }
}
