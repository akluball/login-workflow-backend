package com.gitlab.akluball.lwb.security;

import javax.inject.Singleton;
import java.util.Random;

@Singleton
public class RandomlyCreate {
    private static final String ALPHA_NUMERIC_ALPHABET = "abcdefghijklmnopqrstuvwxyz"
            + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "0123456789";
    private static final String VERIFICATION_CODE_ALPHABET = ALPHA_NUMERIC_ALPHABET;
    private static final int VERIFICATION_CODE_SIZE = 10;
    private static final String SESSION_KEY_ALPHABET = ALPHA_NUMERIC_ALPHABET;
    private static final int SESSION_KEY_SIZE = 10;

    private Random random;

    public RandomlyCreate() {
        this.random = new Random();
    }

    private String randomString(String alphabet, int size) {
        String code = "";
        for (int i = 0; i < size; i++) {
            int nextIndex = this.random.nextInt(alphabet.length());
            code += alphabet.substring(nextIndex, nextIndex + 1);
        }
        return code;
    }

    public String verificationCode() {
        return randomString(VERIFICATION_CODE_ALPHABET, VERIFICATION_CODE_SIZE);
    }

    public String sessionKey() {
        return randomString(SESSION_KEY_ALPHABET, SESSION_KEY_SIZE);
    }
}
