package com.gitlab.akluball.lwb.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

import javax.enterprise.context.Dependent;
import java.util.Date;

public class TokenClaims {
    private final Claims claims;

    private TokenClaims() {
        this.claims = Jwts.claims();
    }

    public Date getExpiration() {
        return this.claims.getExpiration();
    }

    public void loadInto(JwtBuilder jwtBuilder) {
        jwtBuilder.setClaims(this.claims);
    }

    @Dependent
    public static class Builder {
        private final TokenClaims tokenClaims;

        Builder() {
            this.tokenClaims = new TokenClaims();
        }

        public Builder expiration(Date expiration) {
            this.tokenClaims.claims.setExpiration(expiration);
            return this;
        }

        public Builder subject(int subject) {
            this.tokenClaims.claims.setSubject("" + subject);
            return this;
        }

        public TokenClaims build() {
            return this.tokenClaims;
        }
    }
}
