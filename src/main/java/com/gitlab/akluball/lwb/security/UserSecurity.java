package com.gitlab.akluball.lwb.security;

import com.gitlab.akluball.lwb.LoginWorkflowBackendConfig;
import com.gitlab.akluball.lwb.exception.Http401;
import com.gitlab.akluball.lwb.model.User;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.SignatureException;

import javax.inject.Provider;
import javax.inject.Singleton;
import java.security.PrivateKey;

@Singleton
public class UserSecurity {
    private final int tokenLifeSeconds;
    private final PrivateKey privateKey;
    private final JwtParser parser;
    private final Provider<Token.Builder> tokenBuilderProvider;

    UserSecurity(LoginWorkflowBackendConfig loginWorkflowBackendConfig, Provider<Token.Builder> tokenBuilderProvider) {
        this.privateKey = loginWorkflowBackendConfig.privateKey();
        this.parser = Jwts.parser().setSigningKey(loginWorkflowBackendConfig.publicKey());
        this.tokenLifeSeconds = loginWorkflowBackendConfig.tokenLifeSeconds();
        this.tokenBuilderProvider = tokenBuilderProvider;
    }

    public Token createToken(User user) {
        return this.tokenBuilderProvider.get()
                .signWith(this.privateKey)
                .secondsToExpiration(this.tokenLifeSeconds)
                .forUser(user)
                .build();
    }

    public int getVerifiedUserId(String serializedToken) {
        try {
            String subject = this.parser.parseClaimsJws(serializedToken)
                    .getBody()
                    .getSubject();
            return Integer.parseInt(subject);
        } catch (SignatureException | ExpiredJwtException e) {
            throw new Http401("invalid bearer token");
        }
    }
}
