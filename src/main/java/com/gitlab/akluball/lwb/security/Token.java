package com.gitlab.akluball.lwb.security;

import com.gitlab.akluball.lwb.model.User;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

import javax.enterprise.context.Dependent;
import java.security.PrivateKey;
import java.time.Instant;
import java.util.Date;

public class Token {
    private TokenClaims claims;
    private String serialized;

    private Token() {
    }

    public Date getExpiration() {
        return this.claims.getExpiration();
    }

    public String getSerialized() {
        return this.serialized;
    }

    @Dependent
    public static class Builder {
        private final TokenClaims.Builder tokenClaimsBuilder;
        private final JwtBuilder jwtBuilder;

        Builder(TokenClaims.Builder tokenClaimsBuilder) {
            this.tokenClaimsBuilder = tokenClaimsBuilder;
            this.jwtBuilder = Jwts.builder();
        }

        public Builder signWith(PrivateKey privateKey) {
            this.jwtBuilder.signWith(privateKey);
            return this;
        }

        public Builder secondsToExpiration(int seconds) {
            this.tokenClaimsBuilder.expiration(Date.from(Instant.now().plusSeconds(seconds)));
            return this;
        }

        public Builder forUser(User user) {
            user.loadClaims(this.tokenClaimsBuilder);
            return this;
        }

        public Token build() {
            TokenClaims claims = this.tokenClaimsBuilder.build();
            claims.loadInto(this.jwtBuilder);
            Token token = new Token();
            token.serialized = this.jwtBuilder.compact();
            token.claims = claims;
            return token;
        }
    }
}
