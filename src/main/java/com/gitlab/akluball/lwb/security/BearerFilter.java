package com.gitlab.akluball.lwb.security;

import com.gitlab.akluball.lwb.exception.Http401;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.List;

@Provider
@Bearer
public class BearerFilter implements ContainerRequestFilter {
    private UserSecurity userSecurity;
    private javax.inject.Provider<CurrentUserProducer> currentUserProducerProvider;

    public BearerFilter() {
    }

    @Inject
    public BearerFilter(UserSecurity userSecurity,
            javax.inject.Provider<CurrentUserProducer> currentUserProducerProvider) {
        this.userSecurity = userSecurity;
        this.currentUserProducerProvider = currentUserProducerProvider;
    }

    private void bearerAuthRequired() {
        throw new Http401("bearer authorization required");
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        List<String> authorizationHeaders = requestContext.getHeaders().get("Authorization");
        if (authorizationHeaders == null) {
            bearerAuthRequired();
        }
        String serializedToken = authorizationHeaders
                .stream()
                .filter(authHeader -> authHeader.startsWith("Bearer "))
                .map(authHeader -> authHeader.replaceFirst("^Bearer ", ""))
                .findFirst()
                .orElse(null);
        if (serializedToken == null) {
            bearerAuthRequired();
        }
        int userId = this.userSecurity.getVerifiedUserId(serializedToken);
        this.currentUserProducerProvider.get()
                .load(userId);
    }
}
