package com.gitlab.akluball.lwb.security;

import javax.enterprise.context.Dependent;
import javax.ws.rs.core.NewCookie;

@Dependent
public class CookieBuilder {
    private String name;
    private String value;
    private String path;
    private String domain;
    private int maxAgeSeconds;
    private boolean isSecure;
    private boolean isHttpOnly;

    public CookieBuilder name(String name) {
        this.name = name;
        return this;
    }

    public CookieBuilder value(String value) {
        this.value = value;
        return this;
    }

    public CookieBuilder path(String path) {
        this.path = path;
        return this;
    }

    public CookieBuilder domain(String domain) {
        this.domain = domain;
        return this;
    }

    public CookieBuilder maxAgeSeconds(int seconds) {
        this.maxAgeSeconds = seconds;
        return this;
    }

    public CookieBuilder isHttpOnly(boolean isHttpOnly) {
        this.isHttpOnly = isHttpOnly;
        return this;
    }

    public NewCookie build() {
        return new NewCookie(this.name,
                this.value,
                this.path,
                this.domain,
                null,
                this.maxAgeSeconds,
                this.isSecure,
                this.isHttpOnly);
    }
}
