package com.gitlab.akluball.lwb.security;

import com.gitlab.akluball.lwb.data.UserDao;
import com.gitlab.akluball.lwb.exception.Http404;
import com.gitlab.akluball.lwb.model.User;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

@RequestScoped
public class CurrentUserProducer {
    private UserDao userDao;
    private int userId;

    CurrentUserProducer(UserDao userDao) {
        this.userDao = userDao;
    }

    public void load(int userId) {
        this.userId = userId;
    }

    @Produces
    @CurrentUser
    public User currentUser() {
        User user = this.userDao.findById(this.userId);
        if (user == null) {
            throw new Http404(String.format("User with id %s", this.userId));
        }
        return user;
    }
}
