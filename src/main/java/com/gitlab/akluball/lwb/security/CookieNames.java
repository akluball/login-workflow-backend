package com.gitlab.akluball.lwb.security;

public class CookieNames {
    public static final String USER_ID = "login-workflow-user-id";
    public static final String USER_ID_LOGOUT = "login-workflow-user-id-logout";
    public static final String SESSION = "login-workflow-session";
    public static final String SESSION_LOGOUT = "login-workflow-session-logout";
}
