package com.gitlab.akluball.lwb.security;

import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.inject.Singleton;
import javax.persistence.PostLoad;

@Singleton
public class Crypto {
    public String createSalt() {
        return BCrypt.gensalt();
    }

    public String hashPassword(String password, String salt) {
        return BCrypt.hashpw(password, salt);
    }

    @PostLoad
    public void injectCrypto(Client cryptoClient) {
        cryptoClient.injectCrypto(this);
    }

    public interface Client {
        void injectCrypto(Crypto crypto);
    }
}
