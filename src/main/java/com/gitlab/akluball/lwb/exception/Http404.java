package com.gitlab.akluball.lwb.exception;

import javax.ws.rs.core.Response;

public class Http404 extends HttpException {
    public Http404(String reason) {
        super(Response.Status.NOT_FOUND, String.format("not found: %s", reason));
    }
}
