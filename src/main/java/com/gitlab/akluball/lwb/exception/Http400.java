package com.gitlab.akluball.lwb.exception;

import javax.ws.rs.core.Response;

public class Http400 extends HttpException {
    public Http400(String reason) {
        super(Response.Status.BAD_REQUEST, String.format("bad request: %s", reason));
    }
}
