package com.gitlab.akluball.lwb.exception;

import javax.ws.rs.core.Response;

public class Http401 extends HttpException {
    public Http401() {
        super(Response.Status.UNAUTHORIZED, "invalid credentials");
    }

    public Http401(String message) {
        super(Response.Status.UNAUTHORIZED, message);
    }

    public static class Payload implements javax.validation.Payload {}
}
