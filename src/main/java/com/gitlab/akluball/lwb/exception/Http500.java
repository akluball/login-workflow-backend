package com.gitlab.akluball.lwb.exception;

import javax.ws.rs.core.Response;

public class Http500 extends HttpException {
    public Http500() {
        super(Response.Status.INTERNAL_SERVER_ERROR, "internal server error");
    }
}
