package com.gitlab.akluball.lwb.exception;

import javax.ws.rs.core.Response;

public class Http409 extends HttpException {
    public Http409(String reason) {
        super(Response.Status.CONFLICT, String.format("conflict: %s", reason));
    }
}
