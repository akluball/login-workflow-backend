package com.gitlab.akluball.lwb.exception;

import javax.ws.rs.core.Response;

public abstract class HttpException extends RuntimeException {
    private final String message;
    private final Response.Status status;

    public HttpException(Response.Status status, String message) {
        super(message);
        this.status = status;
        this.message = message;
    }

    public Response.Status getStatus() {
        return this.status;
    }

    public String getMessage() {
        return this.message;
    }
}
