package com.gitlab.akluball.lwb.json;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
public class JsonbResolver implements ContextResolver<Jsonb> {
    private final Jsonb jsonb;

    public JsonbResolver() {
        JsonbConfig jsonbConfig = new JsonbConfig()
                .withPropertyVisibilityStrategy(new FieldVisibilityStrategy());
        this.jsonb = JsonbBuilder.create(jsonbConfig);
    }

    @Override
    public Jsonb getContext(Class<?> type) {
        return this.jsonb;
    }
}
