package com.gitlab.akluball.lwb.service;

import com.gitlab.akluball.lwb.data.UserDao;
import com.gitlab.akluball.lwb.model.Registration;

import javax.inject.Singleton;

@Singleton
public class UserService {
    private final UserDao userDao;

    UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public void create(Registration registration) {
        this.userDao.create(registration);
    }
}
