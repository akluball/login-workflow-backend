package com.gitlab.akluball.lwb.service;

import com.gitlab.akluball.lwb.model.MessageBuilder;
import com.gitlab.akluball.lwb.model.Registration;

import javax.inject.Provider;
import javax.inject.Singleton;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import static com.gitlab.akluball.lwb.util.Util.asRuntime;

@Singleton
public class EmailService {
    private final Provider<MessageBuilder> buildAndSendProvider;

    EmailService(Provider<MessageBuilder> buildAndSendProvider) {
        this.buildAndSendProvider = buildAndSendProvider;
    }

    public void sendRegisterVerification(Registration registration) {
        Message message = registration.buildVerificationMessage(this.buildAndSendProvider.get());
        try {
            Transport.send(message);
        } catch (MessagingException e) {
            throw asRuntime(e);
        }
    }
}
