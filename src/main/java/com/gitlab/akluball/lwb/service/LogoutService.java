package com.gitlab.akluball.lwb.service;

import com.gitlab.akluball.lwb.data.UserDao;
import com.gitlab.akluball.lwb.exception.Http400;
import com.gitlab.akluball.lwb.exception.Http404;
import com.gitlab.akluball.lwb.model.User;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Singleton
public class LogoutService {
    private final UserDao userDao;

    @Inject
    LogoutService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Transactional
    public void logout(@NotNull(message = "user id logout cookie is required") Integer userId,
            @NotBlank(message = "session logout cookie is required") String sessionKey) {
        User user = userDao.findById(userId);
        if (Objects.isNull(user)) {
            throw new Http404(String.format("User with id %s", userId));
        }
        if (user.isNotValidSessionKey(sessionKey)) {
            throw new Http400("invalid session");
        }
        user.invalidateSession(sessionKey);
    }
}
