package com.gitlab.akluball.lwb.service;

import com.gitlab.akluball.lwb.data.UserDao;
import com.gitlab.akluball.lwb.exception.Http401;
import com.gitlab.akluball.lwb.model.User;
import com.gitlab.akluball.lwb.security.Token;
import com.gitlab.akluball.lwb.security.UserSecurity;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Singleton
public class TokenService {
    private final UserDao userDao;
    private final UserSecurity userSecurity;

    TokenService(UserDao userDao, UserSecurity userSecurity) {
        this.userDao = userDao;
        this.userSecurity = userSecurity;
    }

    @Transactional
    public String createToken(@NotNull(payload = Http401.Payload.class) Integer userId,
            @NotBlank(payload = Http401.Payload.class) String sessionKey) {
        User user = userDao.findById(userId);
        if (user == null) {
            throw new Http401();
        }
        if (user.isNotValidSessionKey(sessionKey)) {
            throw new Http401();
        }
        Token token = this.userSecurity.createToken(user);
        user.updateSession(sessionKey, token.getExpiration());
        return token.getSerialized();
    }
}
