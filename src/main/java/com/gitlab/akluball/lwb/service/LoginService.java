package com.gitlab.akluball.lwb.service;

import com.gitlab.akluball.lwb.LoginWorkflowBackendConfig;
import com.gitlab.akluball.lwb.data.UserDao;
import com.gitlab.akluball.lwb.exception.Http401;
import com.gitlab.akluball.lwb.model.Session;
import com.gitlab.akluball.lwb.model.User;
import com.gitlab.akluball.lwb.security.CookieBuilder;
import com.gitlab.akluball.lwb.security.CookieNames;
import com.gitlab.akluball.lwb.security.RandomlyCreate;

import javax.inject.Provider;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.core.NewCookie;
import java.time.Instant;
import java.util.Objects;

@Singleton
public class LoginService {
    private final UserDao userDao;
    private final LoginWorkflowBackendConfig loginWorkflowBackendConfig;
    private final Provider<CookieBuilder> cookieBuilderProvider;
    private final RandomlyCreate randomlyCreate;
    private final Provider<Session.Builder> sessionBuilderProvider;
    private final String backendDomain;

    LoginService(UserDao userDao,
            LoginWorkflowBackendConfig loginWorkflowBackendConfig,
            Provider<CookieBuilder> cookieBuilderProvider,
            Provider<Session.Builder> sessionBuilderProvider,
            RandomlyCreate randomlyCreate) {
        this.userDao = userDao;
        this.loginWorkflowBackendConfig = loginWorkflowBackendConfig;
        this.cookieBuilderProvider = cookieBuilderProvider;
        this.randomlyCreate = randomlyCreate;
        this.sessionBuilderProvider = sessionBuilderProvider;
        this.backendDomain = System.getenv("BACKEND_DOMAIN");
    }

    @Transactional
    public NewCookie[] login(@NotBlank(payload = Http401.Payload.class) String identifier,
            @NotBlank(payload = Http401.Payload.class) String password) {
        User user = this.userDao.findByIdentifier(identifier)
                .stream()
                .filter(u -> u.isPassword(password))
                .findFirst()
                .orElse(null);
        if (Objects.isNull(user)) {
            throw new Http401();
        }
        int sessionLifeSeconds = this.loginWorkflowBackendConfig.sessionLifeSeconds();
        CookieBuilder userIdCookieBuilder = this.cookieBuilderProvider.get()
                .name(CookieNames.USER_ID)
                .domain(this.backendDomain)
                .path("/api/token")
                .maxAgeSeconds(sessionLifeSeconds)
                .isHttpOnly(true);
        CookieBuilder userIdLogoutCookieBuilder = this.cookieBuilderProvider.get()
                .name(CookieNames.USER_ID_LOGOUT)
                .domain(this.backendDomain)
                .path("/api/logout")
                .maxAgeSeconds(sessionLifeSeconds)
                .isHttpOnly(true);
        user.loadUserId(userIdCookieBuilder);
        user.loadUserId(userIdLogoutCookieBuilder);
        String sessionKey = this.randomlyCreate.sessionKey();
        NewCookie sessionCookie = this.cookieBuilderProvider.get()
                .name(CookieNames.SESSION)
                .value(sessionKey)
                .domain(this.backendDomain)
                .path("/api/token")
                .maxAgeSeconds(sessionLifeSeconds)
                .isHttpOnly(true)
                .build();
        NewCookie sessionLogoutCookie = this.cookieBuilderProvider.get()
                .name(CookieNames.SESSION_LOGOUT)
                .value(sessionKey)
                .domain(this.backendDomain)
                .path("/api/logout")
                .maxAgeSeconds(sessionLifeSeconds)
                .isHttpOnly(true)
                .build();
        Session session = this.sessionBuilderProvider.get()
                .sessionKey(sessionKey)
                .expiration(Instant.now().plusSeconds(sessionLifeSeconds))
                .build();
        user.addSession(session);
        return new NewCookie[]{
                userIdCookieBuilder.build(),
                userIdLogoutCookieBuilder.build(),
                sessionCookie,
                sessionLogoutCookie
        };
    }
}
