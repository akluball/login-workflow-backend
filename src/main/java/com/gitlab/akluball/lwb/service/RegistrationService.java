package com.gitlab.akluball.lwb.service;

import com.gitlab.akluball.lwb.data.RegistrationDao;
import com.gitlab.akluball.lwb.data.UserDao;
import com.gitlab.akluball.lwb.exception.Http401;
import com.gitlab.akluball.lwb.exception.Http404;
import com.gitlab.akluball.lwb.exception.Http409;
import com.gitlab.akluball.lwb.model.Registration;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Singleton
public class RegistrationService {
    private final RegistrationDao registrationDao;
    private final UserDao userDao;
    private final EmailService emailService;
    private final UserService userService;

    RegistrationService(RegistrationDao registrationDao,
            UserDao userDao,
            EmailService emailService,
            UserService userService) {
        this.registrationDao = registrationDao;
        this.userDao = userDao;
        this.emailService = emailService;
        this.userService = userService;
    }

    @Transactional
    public void create(@Valid Registration.InDto registrationDto) {
        if (Objects.nonNull(this.registrationDao.findByEmail(registrationDto.getEmail()))) {
            throw new Http409(String.format("Registration with email %s", registrationDto.getEmail()));
        }
        if (!this.userDao.findByEmail(registrationDto.getEmail()).isEmpty()) {
            throw new Http409(String.format("User with email %s", registrationDto.getEmail()));
        }
        if (!this.registrationDao.findByHandle(registrationDto.getHandle()).isEmpty()) {
            throw new Http409(String.format("Registration with handle %s", registrationDto.getHandle()));
        }
        if (!this.userDao.findByHandle(registrationDto.getHandle()).isEmpty()) {
            throw new Http409(String.format("User with handle %s", registrationDto.getHandle()));
        }
        Registration registration = this.registrationDao.create(registrationDto);
        this.emailService.sendRegisterVerification(registration);
    }

    @Transactional
    public void verifyRegistration(@NotBlank(payload = Http401.Payload.class) String email,
            @NotBlank(payload = Http401.Payload.class) String verificationCode) {
        Registration registration = this.registrationDao.findByEmail(email);
        if (Objects.isNull(registration)) {
            throw new Http404(String.format("Registration with email: %s", email));
        }
        if (!registration.isVerificationCode(verificationCode)) {
            throw new Http401();
        } else {
            this.userService.create(registration);
            this.registrationDao.delete(registration);
        }
    }
}
