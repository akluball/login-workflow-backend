package com.gitlab.akluball.lwb;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class LoginWorkflowBackend extends Application {
}
