package com.gitlab.akluball.lwb.controller;

import com.gitlab.akluball.lwb.security.CookieNames;
import com.gitlab.akluball.lwb.service.LogoutService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("logout")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LogoutController {
    private final LogoutService logoutService;

    @Inject
    LogoutController(LogoutService logoutService) {
        this.logoutService = logoutService;
    }

    @POST
    public void logout(@CookieParam(CookieNames.USER_ID_LOGOUT) Integer userId,
            @CookieParam(CookieNames.SESSION_LOGOUT) String sessionKey) {
        this.logoutService.logout(userId, sessionKey);
    }
}
