package com.gitlab.akluball.lwb.controller;

import com.gitlab.akluball.lwb.model.Registration;
import com.gitlab.akluball.lwb.service.RegistrationService;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("registration")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RegistrationController {
    private final RegistrationService registrationService;

    RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @POST
    public void create(Registration.InDto registrationDto) {
        this.registrationService.create(registrationDto);
    }

    @POST
    @Path("{email}/verification")
    public void verifyRegistration(@PathParam("email") String email, String verificationCode) {
        this.registrationService.verifyRegistration(email, verificationCode);
    }
}
