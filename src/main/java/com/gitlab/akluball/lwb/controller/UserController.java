package com.gitlab.akluball.lwb.controller;

import com.gitlab.akluball.lwb.model.User;
import com.gitlab.akluball.lwb.security.Bearer;
import com.gitlab.akluball.lwb.security.CurrentUser;
import com.gitlab.akluball.lwb.service.UserService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("user")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {
    private final UserService userService;
    private final Provider<User> currentUserProvider;

    UserController(UserService userService, @CurrentUser Provider<User> currentUserProvider) {
        this.userService = userService;
        this.currentUserProvider = currentUserProvider;
    }

    @GET
    @Bearer
    public User.OutDto get() {
        return this.currentUserProvider.get().toOutDto();
    }
}
