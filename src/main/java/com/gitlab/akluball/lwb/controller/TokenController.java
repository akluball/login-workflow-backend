package com.gitlab.akluball.lwb.controller;

import com.gitlab.akluball.lwb.security.CookieNames;
import com.gitlab.akluball.lwb.service.TokenService;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("token")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TokenController {
    private final TokenService tokenService;

    TokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @GET
    public String get(@CookieParam(CookieNames.USER_ID) Integer userId,
            @CookieParam(CookieNames.SESSION) String sessionKey) {
        return this.tokenService.createToken(userId, sessionKey);
    }
}
