package com.gitlab.akluball.lwb.controller;

import com.gitlab.akluball.lwb.service.LoginService;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

@Path("login/{identifier}")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LoginController {
    private final LoginService loginService;

    LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @POST
    public Response login(@PathParam("identifier") String identifier, String password) {
        NewCookie[] cookies = this.loginService.login(identifier, password);
        return Response.ok().cookie(cookies).build();
    }
}
