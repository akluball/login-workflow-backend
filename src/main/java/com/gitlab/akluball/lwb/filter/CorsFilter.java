package com.gitlab.akluball.lwb.filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

@Provider
public class CorsFilter implements ContainerResponseFilter {
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        String frontendUri = System.getenv("FRONTEND_URI");
        if (frontendUri != null && !frontendUri.isEmpty()) {
            MultivaluedMap<String, Object> headers = responseContext.getHeaders();
            headers.add("Access-Control-Allow-Origin", frontendUri);
            headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,UPDATE,OPTIONS");
            headers.add("Access-Control-Allow-Headers",
                    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization");
            headers.add("Access-Control-Allow-Credentials", true);
        }
    }
}
