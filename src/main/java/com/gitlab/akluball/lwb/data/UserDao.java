package com.gitlab.akluball.lwb.data;

import com.gitlab.akluball.lwb.model.Registration;
import com.gitlab.akluball.lwb.model.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import java.util.List;

@RequestScoped
public class UserDao {
    private final EntityManager entityManager;
    private final Provider<User.Builder> userBuilderProvider;

    UserDao(EntityManager entityManager, Provider<User.Builder> userBuilderProvider) {
        this.entityManager = entityManager;
        this.userBuilderProvider = userBuilderProvider;
    }

    public void create(Registration registration) {
        User user = registration.createUser(this.userBuilderProvider.get());
        this.entityManager.persist(user);
    }

    public User findById(int id) {
        return this.entityManager.find(User.class, id);
    }

    public List<User> findByEmail(String email) {
        return this.entityManager.createNamedQuery(User.Query.Names.FIND_BY_EMAIL, User.class)
                .setParameter(User.Query.Param.EMAIL, email)
                .getResultList();
    }

    public List<User> findByIdentifier(String identifier) {
        return this.entityManager.createNamedQuery(User.Query.Names.FIND_BY_IDENTIFIER, User.class)
                .setParameter(User.Query.Param.IDENTIFIER, identifier)
                .getResultList();
    }

    public List<User> findByHandle(String handle) {
        return this.entityManager.createNamedQuery(User.Query.Names.FIND_BY_HANDLE, User.class)
                .setParameter(User.Query.Param.HANDLE, handle)
                .getResultList();
    }
}
