package com.gitlab.akluball.lwb.data;

import com.gitlab.akluball.lwb.model.Registration;

import javax.enterprise.context.RequestScoped;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import java.util.List;

@RequestScoped
public class RegistrationDao {
    private final Provider<Registration.Builder> registrationBuilderProvider;
    private final EntityManager entityManager;

    RegistrationDao(EntityManager entityManager, Provider<Registration.Builder> registrationBuilderProvider) {
        this.entityManager = entityManager;
        this.registrationBuilderProvider = registrationBuilderProvider;
    }

    public Registration create(Registration.InDto inDto) {
        Registration registration = this.registrationBuilderProvider.get().inDto(inDto).build();
        this.entityManager.persist(registration);
        return registration;
    }

    public Registration findByEmail(String email) {
        return this.entityManager.find(Registration.class, email);
    }

    public List<Registration> findByHandle(String handle) {
        return this.entityManager.createNamedQuery(Registration.Query.Name.FIND_BY_HANDLE, Registration.class)
                .setParameter(Registration.Query.Param.HANDLE, handle)
                .getResultList();
    }

    public void delete(Registration registration) {
        this.entityManager.remove(registration);
    }
}
