package com.gitlab.akluball.lwb.model;

import com.gitlab.akluball.lwb.security.Crypto;
import com.gitlab.akluball.lwb.security.RandomlyCreate;

import javax.enterprise.context.Dependent;
import javax.mail.Message;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity
@Access(AccessType.FIELD)
@NamedQueries(
        @NamedQuery(name = Registration.Query.Name.FIND_BY_HANDLE, query = Registration.Query.FIND_BY_HANDLE)
)
public class Registration {
    @Id
    private String email;
    private String handle;
    private String salt;
    private String passwordHash;
    private String firstName;
    private String lastName;
    private String verificationCode;

    public Message buildVerificationMessage(MessageBuilder messageBuilder) {
        return messageBuilder.to(this.email)
                .subject("Login Workflow Registration Verification")
                .text(this.verificationCode)
                .build();
    }

    public boolean isVerificationCode(String verificationCode) {
        return this.verificationCode.equals(verificationCode);
    }

    public User createUser(User.Builder userBuilder) {
        return userBuilder.email(this.email)
                .handle(this.handle)
                .password(this.salt, this.passwordHash)
                .firstName(this.firstName)
                .lastName(this.lastName)
                .build();
    }

    public static class InDto {
        @NotBlank(message = "email is required")
        @Pattern(
                regexp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
                message = "invalid email"
        )
        private String email;
        @NotBlank(message = "handle is required")
        private String handle;
        @NotBlank(message = "password is required")
        private String password;
        @NotBlank(message = "firstName is required")
        private String firstName;
        @NotBlank(message = "lastName is required")
        private String lastName;

        public String getEmail() {
            return email;
        }

        public String getHandle() {
            return handle;
        }
    }

    @Dependent
    public static class Builder {
        private final Registration registration;
        private final Crypto crypto;

        Builder(Crypto crypto, RandomlyCreate randomlyCreate) {
            this.crypto = crypto;
            this.registration = new Registration();
            this.registration.verificationCode = randomlyCreate.verificationCode();
        }

        public Builder inDto(InDto inDto) {
            this.registration.email = inDto.email;
            this.registration.handle = inDto.handle;
            String salt = crypto.createSalt();
            this.registration.salt = salt;
            this.registration.passwordHash = crypto.hashPassword(inDto.password, salt);
            this.registration.firstName = inDto.firstName;
            this.registration.lastName = inDto.lastName;
            return this;
        }

        public Registration build() {
            return this.registration;
        }
    }

    public static class Query {
        public static final String FIND_BY_HANDLE = "SELECT registration from Registration registration"
                + " WHERE registration.handle=:" + Param.HANDLE;

        public static class Name {
            public static final String FIND_BY_HANDLE = "find_registration_by_handle";
        }

        public static class Param {
            public static final String HANDLE = "handle";
        }
    }
}
