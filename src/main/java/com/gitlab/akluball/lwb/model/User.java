package com.gitlab.akluball.lwb.model;

import com.gitlab.akluball.lwb.security.CookieBuilder;
import com.gitlab.akluball.lwb.security.Crypto;
import com.gitlab.akluball.lwb.security.TokenClaims;

import javax.enterprise.context.Dependent;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.Instant;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "LoginWorkflowUser")
@Access(AccessType.FIELD)
@EntityListeners({ Crypto.class })
@NamedQueries({
        @NamedQuery(name = User.Query.Names.FIND_BY_IDENTIFIER, query = User.Query.FIND_BY_IDENTIFIER),
        @NamedQuery(name = User.Query.Names.FIND_BY_EMAIL, query = User.Query.FIND_BY_EMAIL),
        @NamedQuery(name = User.Query.Names.FIND_BY_HANDLE, query = User.Query.FIND_BY_HANDLE)
})
public class User implements Crypto.Client {
    @Id
    @GeneratedValue
    private int id;
    private String email;
    private String handle;
    private String salt;
    private String passwordHash;
    private String firstName;
    private String lastName;
    private long joinEpochSeconds;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Session> sessions;

    @Transient
    private Crypto crypto;

    @Override
    public void injectCrypto(Crypto crypto) {
        this.crypto = crypto;
    }

    public boolean isPassword(String password) {
        String passedPasswordHashed = this.crypto.hashPassword(password, this.salt);
        return this.passwordHash.equals(passedPasswordHashed);
    }

    public void loadUserId(CookieBuilder userIdCookieBuilder) {
        userIdCookieBuilder.value("" + this.id);
    }

    public void addSession(Session session) {
        this.sessions.add(session);
    }

    public void loadClaims(TokenClaims.Builder claimsAdapterBuilder) {
        claimsAdapterBuilder.subject(this.id);
    }

    public boolean isNotValidSessionKey(String sessionKey) {
        this.sessions.removeIf(Session::isExpired);
        return this.sessions.stream()
                .noneMatch(session -> session.isSessionKey(sessionKey));
    }

    public void updateSession(String sessionKey, Date freshestTokenExpiration) {
        this.sessions.stream()
                .filter(s -> s.isSessionKey(sessionKey))
                .forEach(session -> session.updateFreshest(freshestTokenExpiration));
    }

    public void invalidateSession(String sessionKey) {
        this.sessions = this.sessions.stream()
                .filter(s -> !s.isSessionKey(sessionKey))
                .collect(Collectors.toSet());
    }

    public OutDto toOutDto() {
        return new OutDto(this);
    }

    @Dependent
    public static class Builder {
        private final User user;

        Builder(Crypto crypto) {
            this.user = new User();
            this.user.injectCrypto(crypto);
        }

        public Builder email(String email) {
            this.user.email = email;
            return this;
        }

        public Builder handle(String handle) {
            this.user.handle = handle;
            return this;
        }

        public Builder password(String salt, String passwordHash) {
            this.user.salt = salt;
            this.user.passwordHash = passwordHash;
            return this;
        }

        public Builder firstName(String firstName) {
            this.user.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.user.lastName = lastName;
            return this;
        }

        public User build() {
            this.user.joinEpochSeconds = Instant.now().getEpochSecond();
            return this.user;
        }
    }

    public static class OutDto {
        private final int id;
        private final String handle;
        private final String email;
        private final String firstName;
        private final String lastName;
        private final long joinEpochSeconds;

        private OutDto(User user) {
            this.id = user.id;
            this.handle = user.handle;
            this.email = user.email;
            this.firstName = user.firstName;
            this.lastName = user.lastName;
            this.joinEpochSeconds = user.joinEpochSeconds;
        }
    }

    public static class Query {
        public static final String FIND_BY_IDENTIFIER = "SELECT user FROM User user"
                + " WHERE user.email=:" + Param.IDENTIFIER
                + " OR user.handle=:" + Param.IDENTIFIER;
        public static final String FIND_BY_EMAIL = "SELECT user FROM User user"
                + " WHERE user.email=:" + Param.EMAIL;
        public static final String FIND_BY_HANDLE = "SELECT user FROM User user"
                + " WHERE user.handle=:" + Param.HANDLE;

        public static class Names {
            public static final String FIND_BY_IDENTIFIER = "find_user_by_identifier";
            public static final String FIND_BY_EMAIL = "find_user_by_email";
            public static final String FIND_BY_HANDLE = "find_user_by_handle";
        }

        public static class Param {
            public static final String IDENTIFIER = "identifier";
            public static final String EMAIL = "email";
            public static final String HANDLE = "handle";
        }
    }
}
