package com.gitlab.akluball.lwb.model;

import com.gitlab.akluball.lwb.LoginWorkflowBackendConfig;

import javax.enterprise.context.Dependent;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static com.gitlab.akluball.lwb.util.Util.asRuntime;

@Dependent
public class MessageBuilder {
    private final Message message;

    MessageBuilder(LoginWorkflowBackendConfig loginWorkflowBackendConfig) throws MessagingException {
        MimeMessage mimeMessage = new MimeMessage(loginWorkflowBackendConfig.mailSession());
        mimeMessage.setFrom(loginWorkflowBackendConfig.mailFrom());
        this.message = mimeMessage;
    }

    public MessageBuilder to(String recipientEmail) {
        try {
            this.message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmail));
        } catch (MessagingException e) {
            throw asRuntime(e);
        }
        return this;
    }

    public MessageBuilder subject(String subject) {
        try {
            this.message.setSubject(subject);
        } catch (MessagingException e) {
            throw asRuntime(e);
        }
        return this;
    }

    public MessageBuilder text(String text) {
        try {
            this.message.setText(text);
        } catch (MessagingException e) {
            throw asRuntime(e);
        }
        return this;
    }

    public Message build() {
        return this.message;
    }
}
