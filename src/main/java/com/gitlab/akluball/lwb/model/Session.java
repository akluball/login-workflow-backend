package com.gitlab.akluball.lwb.model;

import javax.enterprise.context.Dependent;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

@Entity
@Access(AccessType.FIELD)
public class Session {
    @Id
    @GeneratedValue
    private int id;
    private String sessionKey;
    private Timestamp expiration;
    private Timestamp freshestTokenExpiration;

    public boolean isSessionKey(String toCheck) {
        return this.sessionKey.equals(toCheck);
    }

    public boolean isExpired() {
        return !this.expiration.toInstant().isAfter(Instant.now());
    }

    public void updateFreshest(Date freshestTokenExpiration) {
        this.freshestTokenExpiration = Timestamp.from(freshestTokenExpiration.toInstant());
    }

    @Dependent
    public static class Builder {
        private final Session session;

        Builder() {
            this.session = new Session();
        }

        public Builder sessionKey(String sessionKey) {
            this.session.sessionKey = sessionKey;
            return this;
        }

        public Builder expiration(Instant expiration) {
            this.session.expiration = Timestamp.from(expiration);
            return this;
        }

        public Session build() {
            return this.session;
        }
    }
}
