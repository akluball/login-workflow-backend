#!/bin/sh

# expectations
# oci-tools repo is rooted at $PWD
# lwb-image/image.tar is rooted at $PWD
# lwb-test-image/image.tar is rooted at $PWD
# postgres/image.tar is rooted at $PWD
# mailhog/image.tar is rooted at $PWD

# install wget for healthchecks
apk add --quiet --no-progress wget

# create keys
# this needs to come before clean-dns
mkdir /keys
private_key_pkcs1_file=/keys/private-pkcs1.pem
private_key_file=/keys/private.pem
public_key_file=/keys/public.pem
apk add --no-cache --quiet --no-progress openssl
openssl genrsa -out "$private_key_pkcs1_file" 2048
openssl rsa -in "$private_key_pkcs1_file" -pubout -out "$public_key_file"
openssl pkcs8 -in "$private_key_pkcs1_file" -topk8 -nocrypt -out "$private_key_file"
private_key="$(cat "$private_key_file")"
public_key="$(cat "$public_key_file")"

# install oci-tools
ln -s "$PWD/oci-tools/lib/oci-tools" "/usr/local/bin"
oci-tools deps apk
oci-tools system mount-cgroups
oci-tools system clean-dns

# create network
network_name=test-net
network_domain=test
oci-tools network create \
    -4 10.0.0.1/24 \
    -6 fd00::1/64 \
    --domain "$network_domain" \
    "$network_name"

# postgres
postgres_tarball="$PWD/postgres/image.tar"
postgres_container=lwb-db
postgres_db=lwb_db
postgres_user=lwb-ci
postgres_password=lwb-ci
postgres_uri="jdbc:postgresql://$postgres_container.test:5432/$postgres_db"
oci-tools bundle extract \
    --format docker \
    --name "$postgres_container" \
    "$postgres_tarball"
# remove /tmp from postgres rootfs
# remount /tmp
# have not figured out a better solution
postgres_bundle_cache="/var/oci-tools/bundles/$postgres_container"
rm -r "$postgres_bundle_cache/rootfs/tmp"
postgres_config_json="$postgres_bundle_cache/config.json"
tmpfs_mount_insert='.mounts=.mounts+[{ "destination": "/tmp", "type": "tmpfs", "source": "tmpfs", "options": [ "mode=1777" ] }]'
temp_json="$(mktemp)"
jq "$tmpfs_mount_insert" "$postgres_config_json" > "$temp_json"
mv "$temp_json" "$postgres_config_json"
oci-tools container run \
    --detach \
    --network "$network_name" \
    --env POSTGRES_DB="$postgres_db" \
    --env POSTGRES_USER="$postgres_user" \
    --env POSTGRES_PASSWORD="$postgres_password" \
    "$postgres_container"

# mailhog
mailhog_tarball="$PWD/mailhog/image.tar"
mailhog_container=lwb-mailhog
oci-tools bundle extract \
    --format docker \
    --name "$mailhog_container" \
    "$mailhog_tarball"
oci-tools container run \
    --detach \
    --network "$network_name" \
    "$mailhog_container"

# backend config
session_life_seconds=3600
token_life_seconds=300

# backend
lwb_tarball="$PWD/lwb-image/image.tar"
lwb_container=lwb-backend
lwb_domain="$lwb_container.$network_domain"
oci-tools bundle extract \
    --format docker \
    --name "$lwb_container" \
    "$lwb_tarball"
oci-tools container run \
    --detach \
    --network "$network_name" \
    --env BACKEND_DOMAIN="$lwb_domain" \
    --env SMTP_HOST="$mailhog_container.test" \
    --env SMTP_PORT=1025 \
    --env SMTP_FROM=lwb-ci@example.com \
    --env POSTGRES_URL="$postgres_uri" \
    --env POSTGRES_USER="$postgres_user" \
    --env POSTGRES_PASSWORD="$postgres_password" \
    --env PRIVATE_KEY="$private_key" \
    --env PUBLIC_KEY="$public_key" \
    --env SESSION_LIFE_SECONDS="$session_life_seconds" \
    --env TOKEN_LIFE_SECONDS="$token_life_seconds" \
    "$lwb_container"

healthcheck() {
    healthcheck_method="$1"
    healthcheck_uri="$2"
    check_count=0
    check_limit=30
    sleep_interval=1
    _healthcheck() {
        wget -q -O /dev/null --method="$healthcheck_method" "$healthcheck_uri" 2> /dev/null
    }
    while [ "$check_count" -lt "$check_limit" ] && ! _healthcheck; do
        check_count=$((check_count + 1))
        sleep "$sleep_interval"
    done
    if [ "$check_count" -eq "$check_limit" ]; then
        printf '%s\n' 'frontend healthcheck failure: may need to increase wait time'
    fi
}

healthcheck GET "$lwb_container:8080/api/healthcheck"

# test
test_tarball="$PWD/lwb-test-image/image.tar"
test_container=lwb-test
oci-tools bundle extract \
    --format docker \
    --name "$test_container" \
    "$test_tarball"
oci-tools container run \
    --network "$network_name" \
    --env BACKEND_DOMAIN="$lwb_domain" \
    --env BACKEND_URI="http://$lwb_container.$network_domain:8080" \
    --env MAILHOG_URI="http://$mailhog_container.$network_domain:8025" \
    --env POSTGRES_URL="$postgres_uri" \
    --env POSTGRES_USER="$postgres_user" \
    --env POSTGRES_PASSWORD="$postgres_password" \
    --env PRIVATE_KEY="$private_key" \
    --env PUBLIC_KEY="$public_key" \
    --env SESSION_LIFE_SECONDS="$session_life_seconds" \
    --env TOKEN_LIFE_SECONDS="$token_life_seconds" \
    "$test_container"
exit_status="$?"

# cleanup
oci-tools container delete "$lwb_container"
oci-tools container delete "$test_container"
oci-tools container delete "$mailhog_container"
oci-tools container delete "$postgres_container"
oci-tools network delete "$network_name"

exit "$exit_status"
