# Login Workflow Backend - Continuous Integration

Continuous integration is performed using [concourse].
A buildkit instance and docker registry are also required.

On a linux host with docker, this infrastructure can be setup for local ci using [ci-side-by-side].

## Infrastructure

To setup with [ci-side-by-side]:
```
sbs network up
sbs concourse up --login <concourse-target>
sbs registry up
sbs buildkit up
```

To teardown:
```
sbs buildkit down
sbs registry down
sbs concourse down
sbs network down
```

## Pipelines

Some of the options have environment defaults:
- `LOGIN_WORKFLOW_CONCOURSE_TARGET` for `<concourse-target>`
- `LOGIN_WORKFLOW_SSH_KEY` for `<ssh-key-file>`
- `SBS_BUILDKIT_ADDR` for `<buildkit-addr>`
- `SBS_REGISTRY` for `<docker-registry>`

If [ci-side-by-side] is used, then the `SBS*` environment variables can be
loaded into the environment with:
```
$(sbs env exports)
```

Create pipeline (branch defaults to current branch):
```
./pipeline-up \
    --target <concourse-target> \
    --key-file <ssh-key-file> \
    --buildkit-addr <buildkit-addr> \
    --registry <docker-registry> \
    BRANCH
```

destroy pipeline (branch defaults to current branch):
```
./pipeline-down \
    --target <concourse-target> \
    BRANCH
```

[concourse]: https://concourse-ci.org/

[ci-side-by-side]: https://gitlab.com/akluball/ci-side-by-side
